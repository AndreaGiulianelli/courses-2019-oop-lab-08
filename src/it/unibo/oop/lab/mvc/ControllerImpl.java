package it.unibo.oop.lab.mvc;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Class that implements Controller
 *
 */
public class ControllerImpl implements Controller{

    private final List<String> strings = new LinkedList<>();
    private String nextString; 

    @Override
    public void setNextString(final String str) {
        this.nextString = Objects.requireNonNull(str, "This method does not accept null strings");
    }

    @Override
    public String getNextString() {
        return this.nextString;
    }

    @Override
    public List<String> stringHistory() {
        return new LinkedList<>(strings);
    }

    @Override
    public void printCurrentString() {
        if (this.nextString != null) {
            System.out.println(this.nextString);
            strings.add(this.nextString);
        } else {
            throw new IllegalStateException("Next String is not set");
        }
    }

}
