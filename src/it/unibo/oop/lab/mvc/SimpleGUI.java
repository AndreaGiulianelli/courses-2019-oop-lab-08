package it.unibo.oop.lab.mvc;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUI {

    private final JFrame frame = new JFrame();

    /*
     * Once the Controller is done, implement this class in such a way that:
     * 
     * 1) I has a main method that starts the graphical application
     * 
     * 2) In its constructor, sets up the whole view
     * 
     * 3) The graphical interface consists of a JTextField in the upper part of the frame, 
     * a JTextArea in the center and two buttons below it: "Print", and "Show history". 
     * SUGGESTION: Use a JPanel with BorderLayout
     * 
     * 4) By default, if the graphical interface is closed the program must exit
     * (call setDefaultCloseOperation)
     * 
     * 5) The behavior of the program is that, if "Print" is pressed, the
     * controller is asked to show the string contained in the text field on standard output. 
     * If "show history" is pressed instead, the GUI must show all the prints that
     * have been done to this moment in the text area.
     * 
     */

    /**
     * @param ctr
     *      controller object
     * builds a new {@link SimpleGUI}.
     */
    public SimpleGUI(final Controller ctr) {
        this.frame.setTitle("MVC");
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        final JTextField newStringField = new JTextField();
        newStringField.setBackground(Color.LIGHT_GRAY);
        final JTextArea historyStrings = new JTextArea();
        historyStrings.setEditable(false);
        panel.add(newStringField, BorderLayout.NORTH);
        panel.add(historyStrings, BorderLayout.CENTER);
        final JPanel secondPanel = new JPanel();
        secondPanel.setLayout(new BoxLayout(secondPanel, BoxLayout.X_AXIS));
        final JButton print = new JButton("Print");
        final JButton history = new JButton("History");
        secondPanel.add(print);
        secondPanel.add(history);
        panel.add(secondPanel, BorderLayout.SOUTH);
        this.frame.setContentPane(panel);
        print.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                try {
                    ctr.setNextString(newStringField.getText());
                    ctr.printCurrentString();
                    newStringField.setText("");;
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        history.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent arg0) {
                final StringBuilder text = new StringBuilder();
                final List<String> history = ctr.stringHistory();
                for (String x : history) {
                    text.append(x);
                    text.append("\n");
                }
                //Cancella l'ultima andata a capo
                if (!history.isEmpty()) {
                    text.deleteCharAt(text.length() - 1);
                }
                historyStrings.setText(text.toString());
            }
        });
        /*
         * Make the frame half the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected.
         * 
         * In order to deal coherently with multimonitor setups, other
         * facilities exist (see the Java documentation about this issue). It is
         * MUCH better than manually specify the size of a window in pixel: it
         * takes into account the current resolution.
         */
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);

        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
        this.frame.setVisible(true);
    }

    /**
     * @param args
     *      args param
     */
    public static void main(final String...args) {
        new SimpleGUI(new ControllerImpl());
    }

}
